#ifndef METAPROG_FUNDAMENTALS_H_JSQ3W9OTOXZD2M7PLDQO
#define METAPROG_FUNDAMENTALS_H_JSQ3W9OTOXZD2M7PLDQO

#pragma once

#include <initializer_list>
#include <type_traits>
#include <utility>

namespace metaprog {
	
  namespace detail {
    // missing in Clang 3.5
    template <class T> using decay_t = typename std::decay<T>::type;
  }

	template <class F, class ... Args>
	constexpr auto for_each_param (F && f, Args && ... args) {
		auto forget = {
			( (void) (std::forward<F>(f) (std::forward<Args> (args))), 0 ) ...
		};
	}
	
	template <class T, T v>
	struct integral_constant {
		constexpr operator T () const { return v; }
		constexpr T operator () () const { return v; }
		constexpr operator std::integral_constant<T,v> () const { return {}; }

    constexpr auto operator + () const { return integral_constant<T, v>{}; }
    constexpr auto operator - () const { return integral_constant<T, -v>{}; }
    constexpr auto operator ~ () const { return integral_constant<T, ~v>{}; }
    constexpr auto operator ! () const { return integral_constant<T, !v>{}; }
	};
	
	using true_type = integral_constant<bool,true>;
	using false_type = integral_constant<bool,false>;
	
	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator + (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1+v2)>, v1+v2>
		{ return {}; }
  
	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator - (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1-v2)>, v1-v2>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator * (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1*v2)>, v1*v2>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator / (integral_constant<T1,v1>, integral_constant<T2,v2>) {
    using result = decltype(v1 / v2);
    return integral_constant<detail::decay_t<result>, v1 / v2>{};
  }
	
	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator % (integral_constant<T1,v1>, integral_constant<T2,v2>) {
    using result = decltype(v1 % v2);
    return integral_constant<detail::decay_t<result>, v1 % v2>{};
  }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator << (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1<<v2)>, (v1<<v2)>
		{ return {}; }
	
	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator >> (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1>>v2)>, (v1>>v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator < (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1<v2)>, (v1<v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator > (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1>v2)>, (v1>v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator <= (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1<=v2)>, (v1<=v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator >= (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1>=v2)>, (v1>=v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator == (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1==v2)>, v1==v2>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator != (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1!=v2)>, v1!=v2>
		{ return {}; }
	
	
	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator & (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1&v2)>, (v1&v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator | (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1|v2)>, (v1|v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator ^ (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1^v2)>, (v1^v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator && (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1&&v2)>, (v1&&v2)>
		{ return {}; }

	template <class T1, T1 v1, class T2, T2 v2>
	constexpr auto operator || (integral_constant<T1,v1>, integral_constant<T2,v2>)
		-> integral_constant<detail::decay_t<decltype(v1||v2)>, v1||v2>
		{ return {}; }
	
	
	
	namespace detail {
    struct int_pair {
      int first, second;
    };
    template <char c> constexpr int_pair static_atoi () {
      return{ c - '0', 1 };
    }
    template <char c1, char c2, char ... c>	constexpr int_pair static_atoi () {
      return{
        10 * (c1 - '0') * static_atoi<c2,c...> ().second + static_atoi<c2,c...> ().first,
        10 * static_atoi<c2,c...> ().second
      };
    }
	}
	
	namespace literals {
		template <char ... c>
		constexpr auto operator "" _c ()
			-> integral_constant<int, detail::static_atoi<c...> ().first >
			{ return {}; }
	}
	
	namespace detail {
		template<class T>
		struct static_const
		{
		    constexpr static T const value {};
			constexpr T const & operator () () const { return value; }
		};

		template<class T>
		constexpr T const static_const<T>::value;		
	}
	
	constexpr static auto const & true_c = detail::static_const<true_type> {} ();
	constexpr static auto const & false_c = detail::static_const<false_type> {} ();
  
  template <class T> struct type {};
  template <template <class ...> class T> struct type_template {};

  template <class T, class Op>
  constexpr auto operator | (type<T> t, Op op) { return op.apply_to (t); }

  template <template <class ...> class T, class Op>
  constexpr auto operator | (type_template<T> t, Op op) { return op.apply_to (t); }

  namespace detail {
    template <class> struct untype_impl;
    template <class T> struct untype_impl<type<T>> { using type = T; };
  }
  template <class T> using untype = typename detail::untype_impl<T>::type;

  namespace detail {
    template <class> struct untype_template_impl;
    template <template <class> class T> struct untype_template_impl<type_template<T>> {
      template <class P> struct untyper { using type = T<P>; };
      template <class P> using type = typename untyper<P>::type;
    };
  }
  template <class T, class P> using untype_template =
    typename detail::untype_template_impl<T>::template untyper<P>::type;

}

#endif

