#ifndef METAPROG_MULTIFUNCTION_H_JSQ3W9OTOXZD2M7PLDQO
#define METAPROG_MULTIFUNCTION_H_JSQ3W9OTOXZD2M7PLDQO

#include <utility>

namespace metaprog {

	namespace detail {
		
		template <class ...> struct Combined;

		template <class T>
		struct Combined <T> : T {
			explicit Combined(T && t) : T(std::forward<T> (t)) {}
			using T::operator ();
		};

		template <class T, class ... U>
		struct Combined <T, U...> : T, Combined<U...> {
			explicit Combined(T && t, U && ... u) : T(std::forward<T> (t)), Combined<U...> (std::forward<U> (u)...) {}
			using T::operator ();
			using Combined<U...>::operator ();
		};
		
	}

	template <class ... T>
	auto combine (T && ... t) {
		return detail::Combined<T ...> {std::forward<T> (t) ...};
	}

}

#endif

