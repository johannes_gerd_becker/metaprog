// tests.cpp : Defines the entry point for the console application.
//

#include "tests.h"
#include "../metaprog/fundamentals.h"
#include "../visualcpp/discover_clone.h"
#include <cassert>
#include <iostream>
#include <vector>

namespace metaprog { namespace tests {

  using test_list_type = std::vector<std::unique_ptr<itest>>;

  static test_list_type & get_test_list () {
    static auto test_list = test_list_type{};
    return test_list;
  }

  void add_test (std::unique_ptr<itest> test) {
    get_test_list().push_back (std::move (test));
  }

  void run_tests () {
    for (auto const & test : get_test_list ()) {
      std::cout << "Running test: " << test->get_name () << "\n";
      test->run ();
    }
  }


}}

//#include <iostream>

constexpr bool require_std_false_type (std::false_type) { return true; }

template<class T>
struct MyTemplatedBase {};

struct MyDecoration {
  template <class D, class B>
  struct type : B {
    using B::B;
    static constexpr int a = 42;
  };
};

template <class D>
struct MyCrtpTemplateBase {
  int getW () const { return static_cast<D const *> (this)->w; }
};

struct MyCrtpDecoration {
  template <class D, class B>
  struct type : B {
    using B::B;
    int w = 20;
  };
};


// Requires:
//   Decoration: Type * Type -> Type, (B, D) => g (B, D)
// Provides:
//   apply_to: [T] => [D => g (T, D)]
//   apply_to: [P => f(P)] => [D => g(f(D), D)]
template <class Decoration>
struct DecorateCrtp {
private:
  // needed to make VC++ 15 happy
  using deco = Decoration;

  template <class P>
  struct result {
    template <class D> using tplt = typename deco::template type<D, P>;
  };

public:
  template <class T>
  constexpr auto apply_to (metaprog::type<T>) const {
    using result_ = result<T>;
    return metaprog::type_template < result_::template tplt > {};
  }

private:

  template <template<class> class T>
  struct result_tplt {
    template <class D> using tplt = typename deco::template type<D,T<D>>;
  };


public:
  template <template <class> class T>
  constexpr auto apply_to (metaprog::type_template<T>) const {
    using result_tplt_ = result_tplt<T>;
    return metaprog::type_template < result_tplt_::template tplt > {};
  }

};

template <class ... P>
struct SpecializeAll {
  template <template <class ...> class T>
  constexpr auto apply_to (metaprog::type_template<T>) const {
    using result = T<P ...>;
    return metaprog::type < result > {};
  }
};

struct ConcludeCrtp {
  template <template <class> class B>
  struct Crtp : B < Crtp<B> > { using base = B<Crtp>; using base::base; };

  template <template <class> class T>
  constexpr auto apply_to (metaprog::type_template<T>) const {
    return metaprog::type <Crtp <T>> {};
  }
};

int main () {
  using namespace metaprog::tests;

  add_simple_test ("Test", []() {});
  run_tests ();

  using metaprog::integral_constant;
  using namespace metaprog::literals;
  using metaprog::true_c;
  using metaprog::false_c;


  static_assert (integral_constant<int, 1> {} == integral_constant<int, 1> {}, "");
  static_assert (integral_constant<int, 1> {} != integral_constant<int, 2> {}, "");
  static_assert (integral_constant<int, 1> {} +integral_constant<int, 2> {} == integral_constant<int, 3> {}, "");

  // test implicit conversion to std::integral_constant
  static_assert (require_std_false_type (false_c), "");

  static_assert (5967_c == 5967, "");

  static_assert (9234_c + 11233_c == 20467_c, "");
  static_assert (12_c != 21_c, "");
  static_assert (21_c - 12_c == 9_c, "");
  static_assert (21_c * 12_c == 252_c, "");
  static_assert (25_c / 12_c == 2_c, "");
  static_assert (25_c % 12_c == 1_c, "");

  static_assert ((25_c << 2_c) == 100_c, "");
  static_assert ((25_c >> 2_c) == 6_c, "");

  static_assert (+12_c == 12_c, "");
  static_assert (4_c - 5_c == -1_c, "");

  static_assert ((!1_c) == 0_c, "");
  static_assert ((~1_c) == integral_constant<int, ~1> {}, "");

  static_assert (4_c < 5_c, "");
  static_assert (!(bool)(4_c < 4_c), "");
  static_assert (!(bool)(5_c < 4_c), "");

  static_assert (5_c > 4_c, "");
  static_assert (!(bool)(4_c > 4_c), "");
  static_assert (!(bool)(4_c > 5_c), "");

  static_assert ((true_c && false_c) == false_c, "");
  static_assert ((true_c || false_c) == true_c, "");
  static_assert ((true_c ^ false_c) == true_c, "");
  static_assert ((true_c ^ true_c) == false_c, "");

  struct MyBase {};
  
  // variable needed to make VC++ 15 happy
  auto tplt_decorated_type = metaprog::type <MyBase>{} | DecorateCrtp<MyDecoration> {};
  using tplt_decorated = decltype (tplt_decorated_type);
  constexpr metaprog::untype_template<tplt_decorated, int> b{};
  static_assert (b.a == 42, "");

  auto tplt_decorated_type2 = metaprog::type_template <MyTemplatedBase>{} | DecorateCrtp<MyDecoration> {};
  using tplt_decorated2 = decltype (tplt_decorated_type2);
  constexpr metaprog::untype_template<tplt_decorated, int> b2{};
  static_assert (b2.a == 42, "");

  auto tplt_specialized_type = metaprog::type_template <MyTemplatedBase>{} | SpecializeAll<int> {};
  using tplt_specialized = decltype (tplt_specialized_type);
  static_assert (std::is_same<metaprog::untype<tplt_specialized>, MyTemplatedBase<int>>::value, "");

  auto crtp_decorated_type = metaprog::type_template <MyCrtpTemplateBase>{}
  | DecorateCrtp<MyCrtpDecoration> {}
  | ConcludeCrtp{};
  using crtp_decorated = decltype (crtp_decorated_type);
  constexpr metaprog::untype<crtp_decorated> b_crtp{};
  assert (b_crtp.getW () == 20);

  return 0;
}
