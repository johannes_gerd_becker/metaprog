
#include "../metaprog/fundamentals.h"
#include "tests.h"

constexpr bool require_std_false_type (std::false_type) { return true; }

namespace metaprog { namespace tests { 

  static auto e = ([] () {
	  using metaprog::integral_constant;
	  using namespace metaprog::literals;
	  using metaprog::true_c;
	  using metaprog::false_c;
	
	
	  static_assert (integral_constant<int,1> {} == integral_constant<int,1> {}, "");
	  static_assert (integral_constant<int,1> {} != integral_constant<int,2> {}, "");
	  static_assert (integral_constant<int,1> {} + integral_constant<int,2> {} == integral_constant<int,3> {}, "");
	
	  // test implicit conversion to std::integral_constant
	  static_assert (require_std_false_type (false_c), "");
	
	  static_assert (5967_c == 5967, "");
	
	  static_assert (9234_c + 11233_c == 20467_c, "");
	  static_assert (12_c != 21_c, "");
	  static_assert (21_c - 12_c == 9_c, "");
	  static_assert (21_c * 12_c == 252_c, "");
	  static_assert (25_c / 12_c == 2_c, "");
	  static_assert (25_c % 12_c == 1_c, "");

	  static_assert ((25_c << 2_c) == 100_c, "");
	  static_assert ((25_c >> 2_c) == 6_c, "");

	  static_assert (+12_c == 12_c, "");
	  static_assert (4_c - 5_c == -1_c, "");

	  static_assert ( (! 1_c) == 0_c, "");
	  static_assert ( (~ 1_c) == integral_constant<int,~1> {}, "");

	  static_assert (4_c < 5_c, "");
	  static_assert (! (bool) (4_c < 4_c), "");
	  static_assert (! (bool) (5_c < 4_c), "");

	  static_assert (5_c > 4_c, "");
	  static_assert (! (bool) (4_c > 4_c), "");
	  static_assert (! (bool) (4_c > 5_c), "");

	  static_assert (true_c && false_c == false_c, "");
	  static_assert (true_c || false_c == true_c, "");
	  static_assert (true_c ^ false_c == true_c, "");
	  static_assert (true_c ^ true_c == false_c, "");

	  return 0;
  }) ();

}}